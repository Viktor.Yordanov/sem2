# TicTacToe

TicTacToe er et klassisk brettspill for to spillere, som spilles på et 3x3 rutenett. 
Spillerne tar tur med å plassere et kryss eller sirkel på brettet. 
Målet med spillet er å få tre like symboler på rad, enten horisontalt, vertikalt eller diagonalt. 
Det er en enkel, men underholdende måte å utfordre venner og familie på, og det tar vanligvis bare noen få minutter å fullføre en runde.



link til video: https://youtu.be/ritx0nz4o8E