package no.uib.inf101.sem2.grid;

import no.uib.inf101.sem2.view.TicTacToeGUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

public class GameBoard{
    private TicTacToeGUI gui;


    /**This is the constructor method that takes an object of the TicTacToeGUI class as an argument.
     * It initializes the gui and board variables and calls the initializeBoard() method to set up 
     * the game board. */
    public GameBoard(TicTacToeGUI gui){
        this.gui = gui;
        
    }


    /**This method checks if there is a winner or if the game ended in a tie. 
     * It adds an ActionListener to the "Yes" and "No" buttons in the TicTacToeGUI class. 
     * It calls the following methods to check for a winner: HorizontalCheck(), VerticalCheck(), 
     * and DiagonalCheck(). If there is no winner, it calls the noWinner() method. */
    public void hasWinner(){
        gui.yes_button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e){
                JOptionPane.getRootFrame().dispose();
                gui.resetBoard();
                gui.hasWinner = false;
            }
        });
    
        gui.no_button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e){
                System.exit(0);
            }
        });

        HorizontalCheck();
        VerticalCheck();
        DiagonalCheck();
        noWinner();
    }

    /**This method checks if any row has the same value for all three buttons. 
     * If it finds a winning combination, it displays a dialog box with the message 
     * "Player X/O has won. Play again?" and clears the counter variable in the TicTacToeGUI class. */
    public void HorizontalCheck(){
        for(int i =0; i<3; i++){
                int j = 0;
                if(gui.board[i][j].getText().equals(gui.currentPlayer) && gui.board[i][j+1].getText().equals(gui.currentPlayer) && gui.board[i][j+2].getText().equals(gui.currentPlayer)){
                    // JOptionPane.showMessageDialog(null, "Player " + currentPlayer + " has won.");
                    JOptionPane.showOptionDialog(null, "Player " + gui.currentPlayer + " has won. Play again?", "Congratulations!",
                    0, 3,null, gui.options, gui.options[0]);
                    gui.hasWinner = true;
                    gui.counter.clear();
                }
            }
    }

    /**This method checks if any column has the same value for all three buttons. If it finds a winning combination,
     * it displays a dialog box with the message "Player X/O has won. Play again?" and clears the counter variable 
     * in the TicTacToeGUI class. */
    public void VerticalCheck(){
        for(int i =0; i<3; i++){
            int j = 0;

            if(gui.board[j][i].getText().equals(gui.currentPlayer) && gui.board[j+1][i].getText().equals(gui.currentPlayer) && gui.board[j+2][i].getText().equals(gui.currentPlayer)){
                JOptionPane.showOptionDialog(null, "Player " + gui.currentPlayer + " has won. Play again?", "Congratulations!",
                JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE,null, gui.options, gui.options[0]);
                gui.hasWinner = true;
                gui.counter.clear();
            }
        }
    }

    /**This method checks if either of the diagonals has the same value for all three buttons. If it finds a winning combination,
     * it displays a dialog box with the message "Player X/O has won. Play again?" and clears the counter variable in the 
     * TicTacToeGUI class. */
    public void DiagonalCheck(){
        if(gui.board[0][0].getText().equals(gui.currentPlayer) && gui.board[1][1].getText().equals(gui.currentPlayer) && gui.board[2][2].getText().equals(gui.currentPlayer)){
            JOptionPane.showOptionDialog(null, "Player " + gui.currentPlayer + " has won. Play again?", "Congratulations!",
            JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE,null, gui.options, gui.options[0]);
            gui.hasWinner = true;
            gui.counter.clear();
        } else if (gui.board[2][0].getText().equals(gui.currentPlayer) && gui.board[1][1].getText().equals(gui.currentPlayer) && gui.board[0][2].getText().equals(gui.currentPlayer)){
            JOptionPane.showOptionDialog(null, "Player " + gui.currentPlayer + " has won. Play again?", "Congratulations!",
            JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE,null, gui.options, gui.options[0]);
            gui.hasWinner = true;
            gui.counter.clear();
        }
    }

    /**This method checks if either of the players has won. If it doesn't find a winning combination,
     * it displays a dialog box with the message "No player has won. Play again?" and clears the counter variable in the 
     * TicTacToeGUI class. */
    public void noWinner(){
        if(gui.counter.size() == 9 && gui.counter.get(8) != "" && gui.hasWinner == false){
            JOptionPane.showOptionDialog(null, "No player has won. Play again?", "Draw!",
            JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE,null, gui.options, gui.options[0]);
            gui.hasWinner = true;
            gui.counter.clear();
        }
    }
}