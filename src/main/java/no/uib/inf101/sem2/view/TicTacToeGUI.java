package no.uib.inf101.sem2.view;

import no.uib.inf101.sem2.grid.GameBoard;

import java.awt.Container;
import java.awt.Font;
import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class TicTacToeGUI extends JFrame{
    public Container pane;
    public JButton [][] board;
    public static String currentPlayer;
    public boolean hasWinner;
    public ArrayList<String> counter = new ArrayList<String>(8);
    public JButton no_button = new JButton("No");
    public JButton yes_button = new JButton("Yes");
    public Object[] options = {yes_button, no_button};

    private GameBoard gameBoard;
     
    /** Constructor method for creating an instance of TicTacToeGUI class. 
     *  It initializes the instance variables, creates a game board using 
     *  GameBoard class, sets the layout of the pane to a 3x3 grid and shows 
     *  the window with specified dimensions and title. */
    public TicTacToeGUI(){
        super();
        pane = getContentPane();
        pane.setLayout(new GridLayout(3,3));

        setTitle("TicTacToe");
        setSize(500,500);
        setResizable(false);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setVisible(true);


        currentPlayer = "x";
        hasWinner = false;
        board = new JButton [3][3];
        initializeBoard();
        gameBoard = new GameBoard(this);
        togglePlayer();
    }

        /**This method resets the game board by setting the text of all the buttons to an empty string. */
        public void resetBoard(){
            for(int i = 0; i<3; i++){
                for(int j = 0; j<3; j++){
                    board[i][j].setText("");
                }
            }
            counter.clear();
            hasWinner = false;
            currentPlayer = "x";
        }
        
        /**This method creates the game board by adding 9 buttons to a panel using a nested for loop. 
         * It sets the font of each button to Sans Serif, bold, and size 100. It also adds an ActionListener
         * to each button to handle clicks, and adds the button to the panel. */
        public void initializeBoard(){
            for(int i = 0; i<3; i++){
                for(int j = 0; j<3; j++){
                    JButton btn = new JButton();
                    btn.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 100));
                    btn.setFocusable(false);
                    board[i][j] = btn;
                    btn.addActionListener(new ActionListener() {
    
                        @Override
                        public void actionPerformed(ActionEvent e){
                            if(((JButton)e.getSource()).getText().equals("")){
                                btn.setText(currentPlayer);
                                counter.add(currentPlayer);
                                hasWinner();
                                togglePlayer();
                            }
                        }  
                    });
                    pane.add(btn);
                }
            }
        }

    /**This method switches the current player from "x" to "o" or vice versa,
     * depending on which player's turn it currently is. */
    public void togglePlayer(){
        if(currentPlayer.equals("x")){
            currentPlayer = "o";
        } else {
            currentPlayer = "x";
        }
    }

    /**This method calls the hasWinner() method of the GameBoard instance. */
    public void hasWinner(){
        gameBoard.hasWinner();
    }
    
    /**This method shows a pop-up message with the name of the player who won 
     * and asks if the players want to play again. */
    public void showWinner(){
        JOptionPane.showOptionDialog(null, "Player " + currentPlayer + " has won. Play again?", "Congratulations!",
            JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE,null, options, options[0]);
    }
    
    /**This method terminates the game and closes the window. */
    public void exitGame(){
        System.exit(0);
    }
}