package no.uib.inf101.sem2;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.awt.Font;

import javax.swing.JButton;
import javax.swing.WindowConstants;

import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.grid.GameBoard;
import no.uib.inf101.sem2.view.TicTacToeGUI;



public class TicTacToeTests {

  @Test
  public void testTicTacToeGUIConstructor() {
    TicTacToeGUI game = new TicTacToeGUI();
    // Check that the game window is displayed
    assertTrue(game.isVisible());
    // Check that the game window has the correct title
    assertEquals("TicTacToe", game.getTitle());
    // Check that the game window is not resizable
    assertFalse(game.isResizable());
    // Check that the game window is closed when the user clicks on the close button
    assertEquals(WindowConstants.DISPOSE_ON_CLOSE, game.getDefaultCloseOperation());
}


/**This test creates a new GameBoard and TicTacToeGUI object before each test case using the @BeforeEach annotation.
 *  Then, in the testInitializeBoard() method, it calls the initializeBoard() method of the gameBoard object and checks 
 * if each button in the board array has the expected font, focusability, and text value. It uses the assertNotNull() and 
 * assertEquals() methods from the JUnit library to check the values of these properties. */
  @Test
  void testInitializeBoard() {
    TicTacToeGUI gui = new TicTacToeGUI();
    GameBoard game = new GameBoard(gui);
    gui.initializeBoard();
      for(int i = 0; i < 3; i++) {
          for(int j = 0; j < 3; j++) {
              JButton button = gui.board[i][j];
              assertNotNull(button);
              assertEquals(button.getFont(), new Font(Font.SANS_SERIF, Font.BOLD, 100));
              assertEquals(button.isFocusable(), false);
              assertEquals(button.getText(), "");
          }
      }
  }


  /**This test creates a new TicTacToeGUI instance and sets some initial state. It then calls resetBoard() and checks that 
   * the state has been reset correctly by asserting that the currentPlayer is "x", hasWinner is false, and counter is empty. */
  @Test
  public void testResetBoard() {
    TicTacToeGUI gui = new TicTacToeGUI();
    gui.currentPlayer = "o";
    gui.hasWinner = true;
    gui.counter.add("x");
    gui.counter.add("o");
    gui.counter.add("x");
    gui.counter.add("x");
    gui.counter.add("o");
    gui.counter.add("o");
    gui.counter.add("x");
    gui.counter.add("x");
    gui.counter.add("o");

    gui.resetBoard();

    assertEquals("x", gui.currentPlayer);
    assertFalse(gui.hasWinner);
    assertTrue(gui.counter.isEmpty());
  }


  /**This test verifies that the togglePlayer() method in the TicTacToeGUI class is working correctly.
   * It initializes a new TicTacToeGUI object, sets the currentPlayer to "x", and calls togglePlayer() twice,
   * verifying that the currentPlayer alternates between "x" and "o" as expected. */
  @Test
  public void testTogglePlayer() {
    TicTacToeGUI gui = new TicTacToeGUI();
    gui.currentPlayer = "x";
    gui.togglePlayer();
    String expectedCurrentPlayer = "o";
    assertEquals(expectedCurrentPlayer, gui.currentPlayer);
    gui.togglePlayer();
    expectedCurrentPlayer = "x";
    assertEquals(expectedCurrentPlayer, gui.currentPlayer);
  }
  

  /**This test verifies that the HorizontalCheck() method in the GameBoard class correctly detects when there is a horizontal
   * line of three matching symbols on the game board. It sets up a new GameBoard object with three "x" symbols in a row on
   * the first row, sets the currentPlayer to "x", and calls the HorizontalCheck() method. It then verifies that the hasWinner 
   * boolean in the TicTacToeGUI object is set to true, indicating that the x player has won. */
  @Test
  public void testHorizontalCheck() {
    TicTacToeGUI gui = new TicTacToeGUI();
    GameBoard game = new GameBoard(gui);
    gui.board[0][0].setText("x");
    gui.board[0][1].setText("x");
    gui.board[0][2].setText("x");
    gui.currentPlayer = "x";
    game.HorizontalCheck();
    boolean expectedHasWinner = true;
    assertEquals(expectedHasWinner, gui.hasWinner);
  }
  

  /**This test verifies that the VerticalCheck() method in the GameBoard class correctly detects when there is a vertical line of 
   * three matching symbols on the game board. It sets up a new GameBoard object with three "o" symbols in a row on the first column,
   * sets the currentPlayer to "o", and calls the VerticalCheck() method. It then verifies that the hasWinner boolean in the 
   * TicTacToeGUI object is set to true, indicating that the o player has won. */
  @Test
  public void testVerticalCheck() {
    TicTacToeGUI gui = new TicTacToeGUI();
    GameBoard game = new GameBoard(gui);
    gui.board[0][0].setText("o");
    gui.board[1][0].setText("o");
    gui.board[2][0].setText("o");
    gui.currentPlayer = "o";
    game.VerticalCheck();
    boolean expectedHasWinner = true;
    assertEquals(expectedHasWinner, gui.hasWinner);
  }
  

  /**This test verifies that the DiagonalCheck() method in the GameBoard class correctly detects when there is a diagonal line of three matching
   * symbols on the game board. It sets up a new GameBoard object with three "x" symbols in a diagonal line from the top left to the bottom right,
   * sets the currentPlayer to "x", and calls the DiagonalCheck() method. It then verifies that the hasWinner boolean in the TicTacToeGUI object
   * is set to true, indicating that the x player has won. */
  @Test
  public void testDiagonalCheck() {
    TicTacToeGUI gui = new TicTacToeGUI();
    GameBoard game = new GameBoard(gui);
    gui.board[0][0].setText("x");
    gui.board[1][1].setText("x");
    gui.board[2][2].setText("x");
    gui.currentPlayer = "x";
    game.DiagonalCheck();
    boolean expectedHasWinner = true;
    assertEquals(expectedHasWinner, gui.hasWinner);
  }
  

  /**This test verifies that the noWinner() method in the GameBoard class correctly detects when there is no winner on the game board.
   * It sets up a new GameBoard object with a mix of "x" and "o" symbols on the game board, calls the noWinner() method, and verifies
   * that the hasWinner boolean in the TicTacToeGUI object is set to false, indicating that there is no winner. */
  @Test
  public void testNoWinner() {
    TicTacToeGUI gui = new TicTacToeGUI();
    GameBoard game = new GameBoard(gui);
    gui.board[0][0].setText("o");
    gui.board[0][1].setText("x");
    gui.board[0][2].setText("o");
    gui.board[1][0].setText("x");
    gui.board[1][1].setText("o");
    gui.board[1][2].setText("x");
    gui.board[2][0].setText("x");
    gui.board[2][1].setText("o");
    gui.board[2][2].setText("x");
    game.noWinner();
    boolean expectedHasWinner = false;
    assertEquals(expectedHasWinner, gui.hasWinner);
  }

}
